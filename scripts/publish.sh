#/bin/bash
version=$(python3 setup.py --version)

if [[ $version == *"dev"* ]]
then
    echo "Published versions must not contain 'dev'."
    exit 1
fi

python setup.py sdist || (echo "Packaging failed" && exit 1)
twine upload dist/*

OnToCode Example Project
------------------------

This repository contains an example that illustrates the use of OnToCode in a
project with multiple components, a web application front end written with
React and the corresponding back end written with the Django REST framework.
Front End and back end are eponymous for the subdirectories in which their
sources are located.

To run this example, Python 3, pip, virtualenv, and npm have to be installed.

For both components, part of the source code is generated from an Ontology that
is located in the ``ontology/`` subdirectory. To generate the code, run the
following commands in both components´ directories:

.. code-block:: bash

   $ virtualenv env # make sure it creates a Python 3 environment
   $ source env/bin/activate
   $ pip install -r requirements.txt
   $ pip install ontocode
   $ python generate.py
   $ deactivate

For the back end, this will generate code in the ``iam/generated/`` directory
based on templates located in the ``src-gen/`` directory. For the front end,
it will generate code in the ``src/generated/`` directory based on templates
located in the ``src-gen/`` directory.

To get the back end server up, run the following command in the ``backend``
directory:

.. code-block:: bash

   $ source env/bin/activate
   $ python manage.py migrate
   $ python manage.py runserver

Now, to test the front end, run the following command in the ``frontend``
directory:

.. code-block:: bash

   $ npm install
   $ npm start

A new browser with the web application running should open.

Copyright and License
---------------------

Copyright © 2019 German Aerospace Center (DLR)

The OnToCode Example Project is licensed under the General Public License
Version 3 or later. Look at the file `LICENSE`, which should be part of any
OnToCode distribution, or visit

    https://www.gnu.org/licenses/gpl-3.0.en.html

for the full text of the license.

Dependencies
~~~~~~~~~~~~

**Python**

The OnToCode Example Project depends on the Python package Django. Django is
published under a BSD license and can be obtained from
https://pypi.org/project/Django/ .

The OnToCode Example Project depends on the Python package django-cors-headers.
django-cors-headers is published under an MIT license and can be obtained from
https://pypi.org/project/django-cors-headers/ .

The OnToCode Example Project depends on the Python package Django REST
framework. Django REST framework is published under a BSD license and can be
obtained from https://pypi.org/project/djangorestframework/ .

The OnToCode Example Project depends on the Python package Jinja2. Jinja2 is
published under a BSD license and can be obtained from
https://pypi.org/project/Jinja2/ .

The OnToCode Example Project depends on the Python package OnToCode. OnToCode is
published under the LGPLv3 and can be obtained from
https://pypi.org/project/ontocode/ .

**JavaScript**

The OnToCode Example Project depends on the JavaScript library Boostrap.
Bootstrap is published under an MIT license and can be obtained from
https://www.npmjs.com/package/bootstrap .

The OnToCode Example Project depends on the JavaScript library React.
React is published under an MIT license and can be obtained from
https://www.npmjs.com/package/react .

The OnToCode Example Project depends on the JavaScript library react-dom.
react-dom is published under an MIT license and can be obtained from
https://www.npmjs.com/package/react-dom .

The OnToCode Example Project depends on the JavaScript library
react-router-dom. react-router-dom is published under an MIT license and can
be obtained from https://www.npmjs.com/package/react-router-dom .

The OnToCode Example Project depends on the JavaScript library react-scripts.
react-scripts is published under an MIT license and can be obtained from
https://www.npmjs.com/package/react-scripts .

The OnToCode Example Project depends on the JavaScript library reactstrap.
reactstrap is published under an MIT license and can be obtained from
https://www.npmjs.com/package/reactstrap .

/*
Copyright, 2019, Deutsches Zentrum für Luft- und Raumfahrt e.V.
Licensed under GPLv3+, see LICENSE for details.
*/
import React from 'react';
import { FormGroup, Input, Label } from 'reactstrap';

export function IdentifiableForm(props) {
  return (
    <form>
      <FormGroup>
        <Label for="identifier">Identifier</Label>
        <Input type="text" name="identifier" placeholder="Identifier"
               value={props.identifiable.identifier}
               onChange={props.onChange}/>
      </FormGroup>
      <FormGroup>
        <Label for="name">Name</Label>
        <Input type="text" value={props.identifiable.name}
               onChange={props.onChange} name="name" placeholder="Name"/>
      </FormGroup>
      <FormGroup>
        <Label for="name">Description</Label>
        <Input type="textarea" value={props.identifiable.description}
               onChange={props.onChange} name="description"
               placeholder="Description"/>
      </FormGroup>
    </form>
  );
}

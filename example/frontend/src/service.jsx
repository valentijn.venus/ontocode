/*
Copyright, 2019, Deutsches Zentrum für Luft- und Raumfahrt e.V.
Licensed under GPLv3+, see LICENSE for details.
*/
const BASE_URL = 'http://localhost:8000/';

export class AssignmentService {
  constructor(assigneeType, assigneeIdentifer, assignedType) {
    this.assigneeType = assigneeType;
    this.assigneeIdentifer = assigneeIdentifer;
    this.assignedType = assignedType;
  }

  async list() {
    const url = `${BASE_URL}${this.assigneeType}s/${this.assigneeIdentifer}/${this.assignedType}s/`;
    const result = await fetch(url);
    return await result.json();
  }

  assign(identifier) {
    return this._update(identifier, 'POST');
  }

  retract(identifier) {
    return this._update(identifier, 'DELETE');
  }

  _update(identifier, method) {
    const url = `${BASE_URL}${this.assigneeType}s/${this.assigneeIdentifer}/${this.assignedType}s/${identifier}/`;
    return fetch(url, { method });
  }

}

export class IdentifiableService {
  constructor(type) {
    this.type = type;
  }

  async list() {
    const result = await fetch(`${BASE_URL}${this.type}s/`);
    return result.json();
  }

  async get(identifier) {
    const result = await fetch(`${BASE_URL}${this.type}s/${identifier}/`);
    return result.json();
  }

  add(identifiable) {
    const url = `${BASE_URL}${this.type}s/`;
    return this._modify(identifiable, url, 'POST');
  }

  update(identifiable) {
    const url = `${BASE_URL}${this.type}s/${identifiable.identifier}/`;
    return this._modify(identifiable, url, 'PUT');
  }

  delete(identifier) {
    return fetch(`${BASE_URL}${this.type}s/${identifier}/`,
                 { method: 'DELETE'});
  }

  _modify(identifiable, url, method) {
    return fetch(url, {
      method,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(identifiable),
    });
  }

}

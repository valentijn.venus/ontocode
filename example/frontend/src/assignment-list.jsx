/*
Copyright, 2019, Deutsches Zentrum für Luft- und Raumfahrt e.V.
Licensed under GPLv3+, see LICENSE for details.
*/
import React, { Component } from 'react';
import { Button, ButtonGroup, ListGroup, ListGroupItem } from 'reactstrap';

import { AssignmentService, IdentifiableService } from './service.jsx';
import { SelectIdentifiableModal } from './select-identifiable-modal.jsx';

export class AssignmentList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      assigneds: [],
    };
    this.assignmentService
      = new AssignmentService(this.props.assigneeType,
                              this.props.assigneeIdentifier,
                              this.props.assignedType);
    this.identifiableService
      = new IdentifiableService(this.props.assignedType);
  }

  componentDidMount() {
    this.loadAssigneds();
  }

  async loadAssigneds() {
    const assigneds = await this.assignmentService.list();
    this.setState({assigneds});
  }

  async loadAssignables() {
    const assignables = await this.identifiableService.list();
    const assignedIdentifiers = this.state.assigneds.map(a => a.identifier);
    return assignables.filter(assignable => {
      return assignedIdentifiers.indexOf(assignable.identifier) < 0;
    });
  }

  async assign(identifier) {
    await this.assignmentService.assign(identifier);
    this.loadAssigneds();
  }

  openAssignModal() {
    const callback = this.assign.bind(this);
    const assignables = this.loadAssignables();
    assignables.then(assignables => {
      this.assignModal.show(this.props.assignedType, assignables, callback);
    });
  }

  async retract(identifier) {
    await this.assignmentService.retract(identifier);
    this.loadAssigneds();
  }

  renderEntry(assigned) {
    const identifier = assigned.identifier;
    return (
      <ListGroupItem key={identifier} className='p-0'>
        <ButtonGroup className='w-100'>
          <span className={'btn w-75 text-left'}>
            {identifier}
          </span>
          <Button className={'w-25'} color='danger'
                  onClick={this.retract.bind(this, identifier)}>
            Retract
          </Button>
        </ButtonGroup>
      </ListGroupItem>
    );
  }

  render() {
    const type = this.props.assignedType;
    const capitalizedType = type.replace(/^(.)/g, l => l.toUpperCase());
    return (
      <div className='px-5 -y-3'>
        <h2>{capitalizedType} Assignments&nbsp;
          <Button color='primary' onClick={this.openAssignModal.bind(this)}>
            Assign
          </Button>
        </h2>
        <ListGroup>
          {this.state.assigneds.map(this.renderEntry.bind(this))}
        </ListGroup>
        <SelectIdentifiableModal ref={(ref) => this.assignModal = ref}/>
      </div>
    );
  }
}

# Copyright, 2019, Deutsches Zentrum für Luft- und Raumfahrt e.V.
# Licensed under GPLv3+, see LICENSE for details.
import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ontocode_example.settings')

application = get_wsgi_application()

# Copyright, 2019, Deutsches Zentrum für Luft- und Raumfahrt e.V.
# Licensed under GPLv3+, see LICENSE for details.
from rest_framework import serializers


class IdentifiableSerializer(serializers.Serializer):
    identifier = serializers.CharField(max_length=40)
    name = serializers.CharField(required=False)
    description = serializers.CharField(required=False)

    def create(self, validated_data):
        return self.Meta.model.objects.create(**validated_data)

    def update(self, instance, validated_data):
        if validated_data['identifier'] != instance.identifier:
            raise serializers.ValidationError('Field "identifier" may not be changed')
        instance.name = validated_data.get('name', None)
        instance.description = validated_data.get('description', None)
        instance.save()
        return instance

# Copyright, 2018-2019, Deutsches Zentrum für Luft- und Raumfahrt e.V.
# Licensed under LGPLv3+, see LICENSE for details.
"""
Library for template-to-code transformation based on data from ontologies
"""
from ontocode.instantiation import *
from ontocode.jinja2_template import *
from ontocode.ontology import *
from ontocode.result_processor import *
from ontocode.query import *
from ontocode.template import *
from ontocode.template_input import *
